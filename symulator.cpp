#include <iostream>
#include <iomanip>
#include <cmath>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_odeiv2.h>
#include "symulator.h"

using namespace std;

 symulator::symulator(double x1, double y1, double z1, double v1, double v2, double v3, double w1, double w2, double w3, double k, double M, double l)
 {
    X = x1;
    Y = y1;
    Z = z1;
    V_x = v1;
    V_y = v2;
    V_z = v3;
    Vw_x = w1;
    Vw_y = w2;
    Vw_z = w3;
    K = k;
    masa = M;
    L = l;
    plik = "wyniki.txt";
 }

 /*Definicja funkcji obliczaj¹cej wektor prawych stron uk³adu równañ*/

int symulator::func (double t, const double y[], double f[],
      void *params)
{
    (void)(t); /* avoid unused parameter warning */
    struct parameter * p = (struct parameter *) params;
    double k = p -> k;
    double M = p -> M;
    double Wx = p -> Wx;
    double Wy = p -> Wy;
    double Wz = p -> Wz;
    f[0] = y[1];
    f[1] = -k/M*(sqrt((y[1]-Wx)*(y[1]-Wx)+(y[3]-Wy)*(y[3]-Wy) + (y[5]-Wz)*(y[5]-Wz))*(y[1]-Wx));
    f[2] = y[3];
    f[3] = -9.80665 -k/M*(sqrt((y[1]-Wx)*(y[1]-Wx)+(y[3]-Wy)*(y[3]-Wy) + (y[5]-Wz)*(y[5]-Wz))*(y[3]-Wy));
    f[4] = y[5];
    f[5] = -k/M*(sqrt((y[1]-Wx)*(y[1]-Wx)+(y[3]-Wy)*(y[3]-Wy) + (y[5]-Wz)*(y[5]-Wz))*(y[5]-Wz));
    return GSL_SUCCESS;
}

/*Definicja funkcji obliczaj¹cej macierz Jacobiego*/

int symulator::jac (double t, const double y[], double *dfdy,
     double dfdt[], void *params)
{
    (void)(t); /* avoid unused parameter warning */
    struct parameter * p = (struct parameter *) params;

    double k = p -> k;
    double M = p -> M;
    double Wx = p -> Wx;
    double Wy = p -> Wy;
    double Wz = p -> Wz;
    gsl_matrix_view dfdy_mat
    = gsl_matrix_view_array (dfdy, 6, 6);
    gsl_matrix * m = &dfdy_mat.matrix;
    gsl_matrix_set (m, 0, 0, 0.0);
    gsl_matrix_set (m, 0, 1, 1.0);
    gsl_matrix_set (m, 0, 2, 0.0);
    gsl_matrix_set (m, 0, 3, 0.0);
    gsl_matrix_set (m, 0, 4, 0.0);
    gsl_matrix_set (m, 0, 5, 0.0);
    gsl_matrix_set (m, 1, 0, 0.0);
    gsl_matrix_set (m, 1, 1, -k/M*(1/(sqrt((y[1]-Wx)*(y[1]-Wx)+(y[3]-Wy)*(y[3]-Wy)*(y[5]-Wz)*(y[5]-Wz)))*(y[1]-Wx)*(y[1]-Wx)+sqrt((y[1]-Wx)*(y[1]-Wx)+(y[3]-Wy)*(y[3]-Wy)*(y[5]-Wz)*(y[5]-Wz))));
    gsl_matrix_set (m, 1, 2, 0.0);
    gsl_matrix_set (m, 1, 3, -k/M*(1/(sqrt((y[1]-Wx)*(y[1]-Wx)+(y[3]-Wy)*(y[3]-Wy)*(y[5]-Wz)*(y[5]-Wz)))*(y[3]-Wy)*(y[1]-Wx)));
    gsl_matrix_set (m, 1, 4, 0.0);
    gsl_matrix_set (m, 1, 5, -k/M*(1/(sqrt((y[1]-Wx)*(y[1]-Wx)+(y[3]-Wy)*(y[3]-Wy)*(y[5]-Wz)*(y[5]-Wz)))*(y[5]-Wz)*(y[1]-Wx)));
    gsl_matrix_set (m, 2, 0, 0.0);
    gsl_matrix_set (m, 2, 1, 0.0);
    gsl_matrix_set (m, 2, 2, 0.0);
    gsl_matrix_set (m, 2, 3, 1.0);
    gsl_matrix_set (m, 2, 4, 0.0);
    gsl_matrix_set (m, 2, 5, 0.0);
    gsl_matrix_set (m, 3, 0, 0.0);
    gsl_matrix_set (m, 3, 1, -k/M*(1/(sqrt((y[1]-Wx)*(y[1]-Wx)+(y[3]-Wy)*(y[3]-Wy)*(y[5]-Wz)*(y[5]-Wz)))*(y[1]-Wx)*(y[3]-Wy)));
    gsl_matrix_set (m, 3, 2, 0.0);
    gsl_matrix_set (m, 3, 3, -k/M*(1/(sqrt((y[1]-Wx)*(y[1]-Wx)+(y[3]-Wy)*(y[3]-Wy)*(y[5]-Wz)*(y[5]-Wz)))*(y[3]-Wy)*(y[3]-Wy)+sqrt((y[1]-Wx)*(y[1]-Wx)+(y[3]-Wy)*(y[3]-Wy)*(y[5]-Wz)*(y[5]-Wz))));
    gsl_matrix_set (m, 3, 4, 0.0);
    gsl_matrix_set (m, 3, 5, -k/M*(1/(sqrt((y[1]-Wx)*(y[1]-Wx)+(y[3]-Wy)*(y[3]-Wy)*(y[5]-Wz)*(y[5]-Wz)))*(y[5]-Wz)*(y[3]-Wy)));
    gsl_matrix_set (m, 4, 0, 0.0);
    gsl_matrix_set (m, 4, 1, 0.0);
    gsl_matrix_set (m, 4, 2, 0.0);
    gsl_matrix_set (m, 4, 3, 0.0);
    gsl_matrix_set (m, 4, 4, 0.0);
    gsl_matrix_set (m, 4, 5, 1.0);
    gsl_matrix_set (m, 5, 0, 0.0);
    gsl_matrix_set (m, 5, 1, -k/M*(1/(sqrt((y[1]-Wx)*(y[1]-Wx)+(y[3]-Wy)*(y[3]-Wy)*(y[5]-Wz)*(y[5]-Wz)))*(y[1]-Wx)*(y[5]-Wz)));
    gsl_matrix_set (m, 5, 2, 0.0);
    gsl_matrix_set (m, 5, 3, -k/M*(1/(sqrt((y[1]-Wx)*(y[1]-Wx)+(y[3]-Wy)*(y[3]-Wy)*(y[5]-Wz)*(y[5]-Wz)))*(y[3]-Wy)*(y[5]-Wz)));
    gsl_matrix_set (m, 5, 4, 0.0);
    gsl_matrix_set (m, 5, 5, -k/M*(1/(sqrt((y[1]-Wx)*(y[1]-Wx)+(y[3]-Wy)*(y[3]-Wy)*(y[5]-Wz)*(y[5]-Wz)))*(y[5]-Wz)*(y[5]-Wz)+sqrt((y[1]-Wx)*(y[1]-Wx)+(y[3]-Wy)*(y[3]-Wy)*(y[5]-Wz)*(y[5]-Wz))));

    dfdt[0] = 0.0;
    dfdt[1] = 0.0;
    dfdt[2] = 0.0;
    dfdt[3] = 0.0;
    dfdt[4] = 0.0;
    dfdt[5] = 0.0;
    return GSL_SUCCESS;
}


int symulator::rozwiaz ()
{

    double k = K;
    double M = masa;
    double Wx = Vw_x;
    double Wy = Vw_y;
    double Wz = Vw_z;
     struct parameter p = {k, M, Wx, Wy, Wz};
    /*Definicja uk³adu równañ, w strukturze znajduj¹ siê kolejno:
    - funkcja obliczaj¹ca prawe strony równania,
    - funkcja obliczaj¹ca macierz Jacobiego,
    - wymiar uk³adu,
    - parametry.*/
    gsl_odeiv2_system sys = {func, jac, 6, &p};
    /*Alokacja struktury steruj¹cej rozwi¹zywaniem uk³adu równañ.
    Argumentami s¹:
    - struktura przechowuj¹ca uk³ad równañ,
    - okreœlenie rodzaju kroku (w tym przypadku metoda Rungego-Kutty
        Prince'a-Dormanda rzêdu 8/9),
    - pocz¹tkowa wartoœæ kroku,
    - dok³adnoœæ wzglêdna,
    - dok³adnoœæ bezwzglêdna.*/

    gsl_odeiv2_driver * d =
    gsl_odeiv2_driver_alloc_y_new (&sys, gsl_odeiv2_step_rk8pd,
                                   1e-6, 1e-6, 0.0);
    double t = 0.0, t1 = L;
    double y[6] = { X, V_x, Y, V_y, Z, V_z };

    cout << setprecision(2) << scientific;

    for (int i = 0; i <= L; i++)
    {
        double ti = i;
        /*Przejœcie sterownikiem d od aktualnej wartoœci t
        do wartoœci ti. Wartoœci pocz¹tkowe w y zast¹pione s¹
        wartoœciami koñcowymi.*/
        int status = gsl_odeiv2_driver_apply (d, &t, ti, y);

        if (status != GSL_SUCCESS)
        {
            cout << "error, return value=" << status << endl;
            break;
        }
        if (y[2]<0) break;
        cout << i << "\t" << y[0] << "\t" << y[1] << "\t" << y[2] << "\t" << y[3] <<  "\t" << y[4] << "\t" << y[5] << endl;
    }

    gsl_odeiv2_driver_free (d);
    return 0;
}
